package com.example.navid.myfirstproject;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.Button;
import android.widget.TextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText txtEnterName;
    TextView txtShowName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnFarsi = (Button) findViewById(R.id.changeToFarsi);
        Button btnEnglish = (Button) findViewById(R.id.changeToEnglish);
        txtEnterName = (EditText) findViewById(R.id.enterText);
        txtShowName = (TextView) findViewById(R.id.showText);
        txtEnterName.addTextChangedListener(watch);

    }


    TextWatcher watch = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            txtShowName.setText(charSequence);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

}
